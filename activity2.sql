SELECT * FROM songs
	LEFT JOIN albums ON albums.id = songs.album_id
	WHERE album_title = "24K Magic" AND songs.genre LIKE "%Funk%";

SELECT albums.album_title, artists.name FROM albums
	LEFT JOIN artists ON albums.artist_id = artists.id
	JOIN songs ON albums.id = songs.album_id
	WHERE songs.length < 300 AND albums.date_released > "2015-01-01";

SELECT artists.name, albums.album_title FROM artists
	LEFT JOIN albums ON artists.id = albums.artist_id
	WHERE artists.name LIKE "%o" AND albums.date_released < '2013-01-01';

SELECT * FROM songs
	LEFT JOIN albums ON songs.album_id = albums.id
	WHERE songs.length > 300 AND songs.length < 400 AND albums.artist_id = 1;

SELECT albums.album_title, albums.date_released FROM albums
	JOIN artists ON artists.id = albums.artist_id
	WHERE albums.album_title LIKE "%born%" AND artists.name LIKE "J%";

SELECT song_name FROM songs
	JOIN albums ON albums.id = songs.album_id
	JOIN artists ON albums.artist_id = artists.id
	WHERE artists.name LIKE "B%";

SELECT song_name FROM songs
	WHERE songs.length > 400;